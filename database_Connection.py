import mariadb
import sys
from user_Object import Benutzer

# Verbindung zur MariaDB-Datenbank herstellen
try:
    connection = mariadb.connect(
        user="hoelto",
        password="Id047r_t1",
        host="5.44.107.229",
        port=3306,
        database="UserAdmin"
    )
except mariadb.Error as e:
    print(f"Fehler beim Verbinden mit der MariaDB-Datenbank: {e}")
    sys.exit(1)

# Cursor erstellen
cursor = connection.cursor()

# SQL-Abfrage ausführen, um Daten abzurufen
cursor.execute("SELECT id, name, age FROM user")  # "alter" in Backticks umschlossen

# Daten abrufen und Objekte erstellen
data_objects = []
for row in cursor.fetchall():
    user = Benutzer(row[0], row[1], row[2])
    data_objects.append(user)

# Verbindung schließen
connection.close()

# Daten ausgeben
for user in data_objects:
    print(f"Id: {user.id}, Name: {user.name}, Alter: {user.age}")